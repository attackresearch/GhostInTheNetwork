package main

import (
	"github.com/google/gopacket/pcap"
	"github.com/songgao/water"
	"log"
	"sync"
)

func readTun(ifce *water.Interface, device string, machine fakeMachine, wg sync.WaitGroup) {
	handle, _ := pcap.OpenLive(device, 65536, true, pcap.BlockForever)
	defer handle.Close()

	defer wg.Done()
	packet := make([]byte, 2000)
	for {
		n, err := ifce.Read(packet)
		if err != nil {
			log.Fatal(err)
		}

		log.Printf("%d sized packet received\n", n)

		//only IPv4 for PoC
		if (packet[0] & 0x60) == 0x40 {
			writePacket(handle, packet[:n], machine)
		}
	}
}
