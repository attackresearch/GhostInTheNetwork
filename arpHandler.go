package main

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
	"log"
	"net"
	"sync"
	"time"
)

type arpEntry struct {
	mac []byte
	sec int64
}

var arptable map[string]string
var arpEntryTable map[string]arpEntry

func arpRequest(handle *pcap.Handle, ip string, machine fakeMachine) []byte {
	// Open up a pcap handle for packet reads/writes.

	broadcastmac, _ := net.ParseMAC("ff:ff:ff:ff:ff:ff")
	nullMac, _ := net.ParseMAC("00:00:00:00:00:00")
	srcMac, _ := net.ParseMAC(machine.FakeMac)
	fakeIP := net.ParseIP(machine.FakeIP)
	targetIP := net.ParseIP(ip)

	eth := layers.Ethernet{
		SrcMAC:       srcMac,
		DstMAC:       broadcastmac,
		EthernetType: layers.EthernetTypeARP,
	}
	arp := layers.ARP{
		AddrType:          layers.LinkTypeEthernet,
		Protocol:          layers.EthernetTypeIPv4,
		HwAddressSize:     6,
		ProtAddressSize:   4,
		Operation:         layers.ARPRequest,
		SourceHwAddress:   srcMac,
		SourceProtAddress: fakeIP.To4(),
		DstHwAddress:      nullMac,
		DstProtAddress:    targetIP.To4(),
	}
	// Set up buffer and options for serialization.
	buffer := gopacket.NewSerializeBuffer()
	opts := gopacket.SerializeOptions{
		FixLengths:       true,
		ComputeChecksums: true,
	}

	gopacket.SerializeLayers(buffer, opts, &eth, &arp)

	outgoingPacket := buffer.Bytes()
	newparsedPacket := gopacket.NewPacket(outgoingPacket, layers.LayerTypeEthernet, gopacket.Default)
	log.Printf("New Arp request: " + newparsedPacket.String())
	handle.WritePacketData(outgoingPacket)

	return checkArp(ip)

}

func arpReply(handle *pcap.Handle, fakeMac net.HardwareAddr, fakeIP []byte, dstMac net.HardwareAddr, dstIP []byte) {
	// Open up a pcap handle for packet reads/writes.

	//respond....
	log.Printf("arpReply: " + fakeMac.String())

	eth := layers.Ethernet{
		SrcMAC:       fakeMac,
		DstMAC:       dstMac,
		EthernetType: layers.EthernetTypeARP,
	}
	arp := layers.ARP{
		AddrType:          layers.LinkTypeEthernet,
		Protocol:          layers.EthernetTypeIPv4,
		HwAddressSize:     6,
		ProtAddressSize:   4,
		Operation:         layers.ARPReply,
		SourceHwAddress:   fakeMac,
		SourceProtAddress: fakeIP,
		DstHwAddress:      dstMac,
		DstProtAddress:    dstIP,
	}
	// Set up buffer and options for serialization.
	buffer := gopacket.NewSerializeBuffer()
	opts := gopacket.SerializeOptions{
		FixLengths:       true,
		ComputeChecksums: true,
	}

	gopacket.SerializeLayers(buffer, opts, &eth, &arp)

	outgoingPacket := buffer.Bytes()
	newparsedPacket := gopacket.NewPacket(outgoingPacket, layers.LayerTypeEthernet, gopacket.Default)
	log.Printf("New Arp: " + newparsedPacket.String())
	handle.WritePacketData(outgoingPacket)

}

func floodarp(handle *pcap.Handle, fakeMac net.HardwareAddr, fakeIP []byte, dstMac net.HardwareAddr, dstIP []byte) {

	for i := 0; i < 10; i++ {
		arpReply(handle, fakeMac, fakeIP, dstMac, dstIP)
		time.Sleep(100 * time.Millisecond)
	}
}

func checkArp(ip string) []byte {
	timeout := time.After(5 * time.Second)
	ticker := time.Tick(500 * time.Millisecond)
	// Keep trying until we're timed out or get a result/error
	for {
		select {
		// Got a timeout! fail with a timeout error
		case <-timeout:
			return nil
		// Got a tick, we should check on checkSomething()
		case <-ticker:
			now := time.Now()
			//So basically if the IP is in the arp Entry Table, and is less than 10 minutes old, we will assign it, otherwise query it
			if arpInfo, ok := arpEntryTable[ip]; ok && arpInfo.sec > now.Unix()-600 {
				return arpInfo.mac
			}

			// checkArp() isn't done yet, but it didn't fail either, let's try again
		}
	}
}

func readArp(device string, wg sync.WaitGroup, machine fakeMachine) error {
	defer wg.Done()
	// Open up a pcap handle for packet reads/writes.
	handle, err := pcap.OpenLive(device, 65536, true, pcap.BlockForever)
	if err != nil {
		return err
	}
	defer handle.Close()
	err = handle.SetBPFFilter("arp")
	if err != nil {
		log.Fatal(err)
	}

	src := gopacket.NewPacketSource(handle, layers.LayerTypeEthernet)
	in := src.Packets()
	for {
		var packet gopacket.Packet
		select {
		case packet = <-in:
			arpLayer := packet.Layer(layers.LayerTypeARP)
			if arpLayer == nil {
				continue
			}
			arp := arpLayer.(*layers.ARP)
			if arp.Operation == layers.ARPReply {
				//will capture all arps, including from the base.... If this isn't desired add an if the dst ip is in the arptable
				now := time.Now()
				arpEntryTable[net.IP(arp.SourceProtAddress).String()] = arpEntry{arp.SourceHwAddress, now.Unix()}
				log.Printf("Arp reply added %s [%s]@%d", net.IP(arp.SourceProtAddress).String(), string(arp.SourceHwAddress), now.Unix())
			}
			if arp.Operation == layers.ARPRequest {
				if machine.arpRespond {
					if net.IP(arp.DstProtAddress).String() == machine.FakeIP {
						//respond....
						etherLayer := packet.Layer(layers.LayerTypeEthernet)
						parsedEther, _ := etherLayer.(*layers.Ethernet)

						fakeMac, _ := net.ParseMAC(machine.FakeMac)

						eth := layers.Ethernet{
							SrcMAC:       fakeMac,
							DstMAC:       parsedEther.SrcMAC,
							EthernetType: layers.EthernetTypeARP,
						}
						arp := layers.ARP{
							AddrType:          layers.LinkTypeEthernet,
							Protocol:          layers.EthernetTypeIPv4,
							HwAddressSize:     6,
							ProtAddressSize:   4,
							Operation:         layers.ARPReply,
							SourceHwAddress:   fakeMac,
							SourceProtAddress: arp.DstProtAddress,
							DstHwAddress:      parsedEther.SrcMAC,
							DstProtAddress:    arp.SourceProtAddress,
						}
						// Set up buffer and options for serialization.
						buffer := gopacket.NewSerializeBuffer()
						opts := gopacket.SerializeOptions{
							FixLengths:       true,
							ComputeChecksums: true,
						}

						gopacket.SerializeLayers(buffer, opts, &eth, &arp)

						outgoingPacket := buffer.Bytes()
						newparsedPacket := gopacket.NewPacket(outgoingPacket, layers.LayerTypeEthernet, gopacket.Default)
						log.Printf("New Arp: " + newparsedPacket.String())
						handle.WritePacketData(outgoingPacket)
					}
				}
			}
		}
	}

}
