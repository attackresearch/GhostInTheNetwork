# Ghost In The Network (gitn)
## Building gitn

Gitn will require that you have libpcap-dev, and golang. Bast that, go will take care of the rest, just run:

```
go build 
```
This will create the GhostInTheNetwork binary for use on the platform. This can be crosscompiled with go, but that is left upto the reader, as it can be different depending on platform, target, etc.

# Running gitn

Gitn must be run as root, as it will need raw access to the network both for sniffing, and crafting packets. When using gitn, there is a help, as seen below.

```
Usage of ./GhostInTheNetwork:
  -arp
        Respond to arp requests (For Testing) (default true)
  -fakeIP string
        The IP of the ghost machine on the remote network
  -fakeMac string
        The MAC address of the ghost machine on the remote network (default "ff:ff:ff:ff:ff:ff")
  -i string
        Interface to put the fake machine on (default "en0")
  -routerMac string
        Send all traffic to this target MAC address
  -tunIP string
        TUN Client IP, this will be what is replaced with fakeIP (default "10.22.33.44")
```

The most important feilds are the interface, the fakeMac, and the fakeIP. These feilds will really help craft the packets the way you want them on the correct network. 

Here is an example of setting up gitn to spoof on en0 as 192.168.1.1 with the broadcast mac and sending traffic through the router 00:11:22:33:44:55:

```
./GhostInTheNetwork -fakeIP 192.168.1.1 -fakeMac 00:00:00:00:00:00 -routerMac 00:11:22:33:44:55
```

When running gitn, it is important setup the tun, and then to then route the traffic you want spoofed over the TUN device. 

On linux that would look something like the following:

```
ip addr add 10.22.33.44/32 dev tun0
ip link set tun0 up
ip route add 192.168.1.1/32 dev tun0
```

On mac this would look like:

```
ifconfig utun11 10.22.33.44 10.22.33.44 up
route add -host 192.168.128.4 -interface utun11
```

Note that this was tested mostly on wired side of the network.

Blog post with more details available at https://www.attackresearch.com/post/ghost-in-the-network-mac-spoofing-for-fun

