package main

import (
	"flag"
	"github.com/songgao/water"
	"log"
	"sync"
)

type fakeMachine struct {
	FakeIP     string
	FakeMac    string
	TargetMac  string // only used if router is 3
	ClientIP   string // what is the tun set to
	arpRespond bool
}

func main() {
	var connectionInfo fakeMachine
	var wg sync.WaitGroup

	flag.StringVar(&connectionInfo.FakeIP, "fakeIP", "", "The IP of the ghost machine on the remote network")
	flag.StringVar(&connectionInfo.FakeMac, "fakeMac", "ff:ff:ff:ff:ff:ff", "The MAC address of the ghost machine on the remote network")
	flag.StringVar(&connectionInfo.TargetMac, "routerMac", "", "Send all traffic to this target MAC address")
	flag.StringVar(&connectionInfo.ClientIP, "tunIP", "10.22.33.44", "TUN Client IP, this will be what is replaced with fakeIP")
	flag.BoolVar(&connectionInfo.arpRespond, "arp", true, "Respond to arp requests (For Testing)")

	dev := flag.String("i", "en0", "Interface to put the fake machine on")
	flag.Parse()

	//initialize arp map
	arptable = make(map[string]string)
	arpEntryTable = make(map[string]arpEntry)

	//add to arp table
	arptable[connectionInfo.FakeMac] = connectionInfo.FakeIP
	if connectionInfo.FakeIP == "" {
		log.Fatal("fakeIP must be set, autopick is not supported.")
	}

	log.Printf("Starting with the following:")
	log.Printf("\tfakeIP: " + connectionInfo.FakeIP)
	log.Printf("\tfakeMac: " + connectionInfo.FakeMac)
	log.Printf("\trouterMac: " + connectionInfo.TargetMac)
	log.Printf("\ttunIP: " + connectionInfo.ClientIP)
	if connectionInfo.arpRespond {
		log.Printf("\tarp: True")
	} else {
		log.Printf("\tarp: False")
	}

	ifce, err := water.New(water.Config{
		DeviceType: water.TUN,
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Interface Name: %s\n\n", ifce.Name())
	log.Printf("Setup the interface with the IP %s, and route traffic through as desired", connectionInfo.ClientIP)
	log.Printf("Run the following commands to turn on the interface, and set the IP on linux:")
	log.Printf("\tip addr add %s/32 dev %s; ip link set %s up\n\n", connectionInfo.ClientIP, ifce.Name(), ifce.Name())
	log.Printf("For any machines you want to talk to on linux run:\n\tip route add {IP} dev %s\n", ifce.Name())

	wg.Add(3)
	go readTun(ifce, *dev, connectionInfo, wg)
	go sniffer(*dev, ifce, connectionInfo, wg)
	go readArp(*dev, wg, connectionInfo)
	wg.Wait()
}
