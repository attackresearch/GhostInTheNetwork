package main

import (
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
	"github.com/songgao/water"
	"log"
	"net"
	"sync"
	"time"
)

func sniffer(device string, ifce *water.Interface, machine fakeMachine, wg sync.WaitGroup) {
	serializeOpts := gopacket.SerializeOptions{
		FixLengths:       true,
		ComputeChecksums: true,
	}
	defer wg.Done()
	handle, err := pcap.OpenLive(device, 65536, true, pcap.BlockForever)
	if err != nil {
		log.Fatal(err)
	}
	defer handle.Close()
	filter := fmt.Sprintf("dst host %s", machine.FakeIP) //grabbing both to pass them across the interface

	err = handle.SetBPFFilter(filter)
	if err != nil {
		log.Fatal(err)
	}
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
	for packet := range packetSource.Packets() {
		//Capture Packet
		capEthernetLayer := packet.Layer(layers.LayerTypeEthernet)
		if capEthernetLayer != nil {
			capIpLayer := packet.Layer(layers.LayerTypeIPv4)
			if capIpLayer == nil {
				continue
			}
			newipv4, _ := capIpLayer.(*layers.IPv4)
			newipv4.DstIP = net.ParseIP(machine.ClientIP)

			//Grab TCP/UDP, ignore others for now....
			if newipv4.Protocol == layers.IPProtocolTCP {
				//grab tcp layer
				tcplayer := packet.Layer(layers.LayerTypeTCP)
				parserdTcp := tcplayer.(*layers.TCP)
				data := parserdTcp.Payload

				parserdTcp.SetNetworkLayerForChecksum(newipv4)

				buffer := gopacket.NewSerializeBuffer()

				buffer = gopacket.NewSerializeBuffer()
				gopacket.SerializeLayers(buffer, serializeOpts,
					newipv4,
					parserdTcp,
					gopacket.Payload(data),
				)
				outgoingPacket := buffer.Bytes()
				newparsedPacket := gopacket.NewPacket(outgoingPacket, layers.LayerTypeIPv4, gopacket.Default)
				log.Printf("Raw New: % x \n", outgoingPacket)
				log.Printf("New Packet: " + newparsedPacket.String())
				ifce.Write(outgoingPacket)
			}
			if newipv4.Protocol == layers.IPProtocolUDP {
				//grab tcp layer
				udpLayer := packet.Layer(layers.LayerTypeUDP)
				parserdUDP := udpLayer.(*layers.UDP)
				data := parserdUDP.Payload

				parserdUDP.SetNetworkLayerForChecksum(newipv4)

				buffer := gopacket.NewSerializeBuffer()

				buffer = gopacket.NewSerializeBuffer()
				gopacket.SerializeLayers(buffer, serializeOpts,
					newipv4,
					parserdUDP,
					gopacket.Payload(data),
				)
				outgoingPacket := buffer.Bytes()
				newparsedPacket := gopacket.NewPacket(outgoingPacket, layers.LayerTypeIPv4, gopacket.Default)
				log.Printf("New Packet: " + newparsedPacket.String())
				ifce.Write(outgoingPacket)

			}
		}

	}
}

func writePacket(handle *pcap.Handle, packet []byte, machine fakeMachine) {
	parsedPacket := gopacket.NewPacket(packet, layers.LayerTypeIPv4, gopacket.Default)
	log.Printf("Original Packet:\n " + parsedPacket.String())

	ipv4Packet := parsedPacket.Layer(layers.LayerTypeIPv4)

	serializeOpts := gopacket.SerializeOptions{
		FixLengths:       true,
		ComputeChecksums: true,
	}

	//change IP
	newipv4, _ := ipv4Packet.(*layers.IPv4)

	newipv4.SrcIP = net.ParseIP(machine.FakeIP)

	//new ethernet layer
	srcmac, _ := net.ParseMAC(machine.FakeMac)
	var TargetMac []byte
	TargetMac = nil
	if machine.TargetMac == "" {
		now := time.Now()
		//So basically if the IP is in the arp Entry Table, and is less than 10 minutes old, we will assign it, otherwise query it
		if arpInfo, ok := arpEntryTable[newipv4.DstIP.String()]; ok && arpInfo.sec > now.Unix()-600 {
			TargetMac = arpInfo.mac
		} else {
			TargetMac = arpRequest(handle, newipv4.DstIP.String(), machine)
		}
	} else {
		TargetMac, _ = net.ParseMAC(machine.TargetMac)
	}
	if TargetMac == nil {
		// TODO Craft an ICMP No Route to host back to the TUN
		log.Printf("No ARP for the IP %s, can't send packet", newipv4.DstIP)
		return
	}
	ethernetLayer := &layers.Ethernet{
		SrcMAC:       srcmac,
		DstMAC:       TargetMac,
		EthernetType: layers.EthernetTypeIPv4,
	}

	//Grab TCP/UDP, ignore others for now....
	if newipv4.Protocol == layers.IPProtocolTCP {
		//grab tcp layer
		tcplayer := parsedPacket.Layer(layers.LayerTypeTCP)
		parserdTcp := tcplayer.(*layers.TCP)
		data := parserdTcp.Payload

		parserdTcp.SetNetworkLayerForChecksum(newipv4)

		if parserdTcp.SYN {
			go floodarp(handle, srcmac, newipv4.SrcIP.To4(), TargetMac, newipv4.DstIP.To4())
		}

		buffer := gopacket.NewSerializeBuffer()

		buffer = gopacket.NewSerializeBuffer()
		gopacket.SerializeLayers(buffer, serializeOpts,
			ethernetLayer,
			newipv4,
			parserdTcp,
			gopacket.Payload(data),
		)
		outgoingPacket := buffer.Bytes()
		newparsedPacket := gopacket.NewPacket(outgoingPacket, layers.LayerTypeEthernet, gopacket.Default)
		log.Printf("Raw New: % x \n", outgoingPacket)
		log.Printf("New Packet: " + newparsedPacket.String())

		handle.WritePacketData(outgoingPacket)
	}
	if newipv4.Protocol == layers.IPProtocolUDP {
		//grab tcp layer
		udpLayer := parsedPacket.Layer(layers.LayerTypeUDP)
		parserdUdp := udpLayer.(*layers.UDP)
		data := parserdUdp.Payload

		parserdUdp.SetNetworkLayerForChecksum(newipv4)

		buffer := gopacket.NewSerializeBuffer()

		buffer = gopacket.NewSerializeBuffer()
		gopacket.SerializeLayers(buffer, serializeOpts,
			ethernetLayer,
			newipv4,
			parserdUdp,
			gopacket.Payload(data),
		)
		outgoingPacket := buffer.Bytes()
		newparsedPacket := gopacket.NewPacket(outgoingPacket, layers.LayerTypeEthernet, gopacket.Default)
		log.Printf("New Packet: " + newparsedPacket.String())

		handle.WritePacketData(outgoingPacket)
	}

}
